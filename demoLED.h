/**
 * @file demoLED.h
 *
 * @defgroup CAN Module Support Files
 *
 * @date Apr 16, 2014
 * @author: Economos, Steven J
 * @version 1.0
 *
 * @bief Liability and Copyright Information
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 * 
 */

#ifndef _DEMOLED_H_
#define __DEMOLED_H__
/**
* @brief RED_LED_ON function that writes the Red_LED command using GPIOPinWrite 
*/
void RED_LED_ON();
 /**
* @brief BLUE_LED_ON function that writes the BLUE_LED command using GPIOPinWrite 
*/
void BLUE_LED_ON();
 /**
* @brief GREEN_LED_ON function that writes the GREEN_LED command using GPIOPinWrite 
*/
void GREEN_LED_ON();
 /**
* @brief demoLED Function that creates the timed tasks that trigger the writes to the GPIOPinWite function 
*
*/
void demoLED();
#endif
