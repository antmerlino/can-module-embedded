/**
 * @file MER_game1.h
 *
 * @defgroup mer_game1 Snake
 * @ingroup games
 *
 *
 * Note: the boundary is drawn with extended ASCII chars, to view it properly
 * change translation in PuTTY:
 * Change Settings -> Window -> Translation
 * Then set the Remote character set to CP866
 *
 * This game is a simple snake game.  Use the WASD keys to control
 * the movement of your snake.  Like the classic, travel over a piece
 * of fruit and your snake will eat it and grow larger.
 *
 * This game supports multi-player.  To add a snake, you must
 *
 * "$MUH3 play" will start the game.
 * "$MUH3 help" will print information about the game.
 *
 * Enjoy!
 *
 *  Created on: APR 11, 2014
 *      Author: Anthony Merlino
 *
 *  @{
 */


#ifndef MER_GAME1_H_
#define MER_GAME1_H_


enum snake_fcn
{
	SNAKE_BASE = 0,
	REQUEST,
	JOIN,
	GAMEINFO,
	PLAY,
	MOVE,
	FRUIT,
	UPDATE,
	GAMEOVER,

	SNAKE_TOP
};

typedef enum {
	UINITIALIZED = 0,
	REQUESTING,
	WAITING,
	PLAYING,
} game_status_t;


typedef enum{
	RESET = -1,
	UP,
	DOWN,
	LEFT,
	RIGHT,
	DEAD
}direction_t ;

/**
 *  MER_Init1 must be called before the game can be played
 */
void Snake_MasterInit(void);

void PlaySnake(void);

void Snake_AddPlayer(uint8_t addr);

void SetFruit(uint8_t x, uint8_t y);

void PushFruit(void);

game_status_t Snake_GetStatus(void);

void Snake_UpdateGame(void);

void Snake_RequestReceiver(char c);

void Snake_SetMaster(uint8_t addr);

void SnakeMove(uint8_t addr, direction_t direction);

void Snake_GameOver(void);

void PushFruit(void);

#endif /* MER_GAME1_H_ */
