/**
 * @file demoLED.c
 *
 * @defgroup CAN Module Support Files
 *
 * @date Apr 16, 2014
 * @author: Economos, Steven J
 * @version 1.0
 *
 * @bief Liability and Copyright Information
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 * 
 */
#define RED_LED   GPIO_PIN_1
#define BLUE_LED  GPIO_PIN_2
#define GREEN_LED GPIO_PIN_3
#include "task.h"
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_gpio.h"
#include "inc/hw_ints.h"

#include "inc/hw_sysctl.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"


static int n = 0;


/**
* @brief RED_LED_ON function that writes the Red_LED command using GPIOPinWrite 
*/
 void RED_LED_ON(){
	if(n>10){
	 		RemoveTask(RED_LED_ON);
	 		}
	 GPIOPinWrite(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED, RED_LED);

 }
 /**
* @brief BLUE_LED_ON function that writes the BLUE_LED command using GPIOPinWrite 
*/
 void BLUE_LED_ON(){
	if(n>10){
		 	RemoveTask(BLUE_LED_ON);
		 	}
	GPIOPinWrite(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED, BLUE_LED);

}
/**
* @brief GREEN_LED_ON function that writes the GREEN_LED command using GPIOPinWrite 
*/
 void GREEN_LED_ON(){
	n++;
	 if(n>10){
		 	RemoveTask(GREEN_LED_ON);
		 	GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED);
		 	}
	GPIOPinWrite(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED, GREEN_LED);

}
/**
* @brief demoLED Function that creates the timed tasks that trigger the writes to the GPIOPinWite function 
*
*/
void demoLED(){
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, RED_LED|BLUE_LED|GREEN_LED);
	n=0;
	TaskScheduleAdd(RED_LED_ON,TASK_LOW_PRIORITY,0,1500);
	TaskScheduleAdd(GREEN_LED_ON,TASK_LOW_PRIORITY,500,1500);
	TaskScheduleAdd(BLUE_LED_ON,TASK_LOW_PRIORITY,1000,1500);

}
