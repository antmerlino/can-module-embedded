/*
 * cansys.c
 *
 *  Created on: Apr 15, 2014
 *      Author: Jehandad, Merlino, Muhlbaier
 */

#include "subsys.h"
#include "cansys.h"
#include "CAN_cmds.h"
#include "ADC_ReadChannel.h"
#include "ADCReadTemperature.h"
#include <string.h>
#include "CAN_SNAKE.h"

//This variable will hold the address that we succeed in getting
static uint8_t myaddress = 0xFF;
#ifdef CAN_MASTER
static uint8_t addrCount = 0;
#endif
//tCANMsgObject TxFunc_Msg; //Structure for the Func transmission msg object

//tCANMsgObject Msg3; //Structure for the Addr Tx MO

// used to track which message objects are used
uint32_t message_objects_used;

uint8_t txData[8]; // int8_tacter arrays to hold the data for the above message objects
uint8_t rxData[8];
uint8_t TxAddr_Data[8];

#define CAN_TX_TIMEOUT_MS 1000

volatile bool bErrFlag = 0;

//void CANTick(void); // This function should be a scheduled task and be run whenever the system is idle
void CAN_Handler(void);
void CAN_Callback(char * str);
void CAN_ParseAddr(uint8_t from, uint8_t * data);

static uint8_t checkMsgRxStatus(uint8_t MsgNum);
static uint8_t checkMsgTxStatus(uint8_t MsgNum);
uint8_t CAN_GetMyAddress(void) { return myaddress; };

void CAN_Init(void)
{
	version_t v;
	v.word = CAN_VERSION;
	// Register the module with the system and give it the name "MUH3"
	SubsystemInit(CAN, MESSAGE, "CAN", v);
	LogMsg(CAN, FORCE_LOG, "CAN subsystem initialized, VERBOSE_MESSAGES supressed. To show type $lev -s%d -l%d", CAN, EVERYTHING);

	// Register a callback with the system. After this any commands sent to
	// the system that start with $CAN will be sent to this module
	RegisterCallback(CAN, CAN_Callback);

	tCANMsgObject RxFunc_Msg; //Structure for the Func receive msg object
	tCANMsgObject RxAddr_Msg; //Structure for the Addr Rx MO

	// Enable register access to CAN1 Module
	SysCtlPeripheralEnable(SYSCTL_PERIPH_CAN0);

	// Enable the GPIO port A
	SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOE );
	SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOB );

	// Configure PA0 as CAN1 Rx
	GPIOPinConfigure(GPIO_PE4_CAN0RX);

	// Configure PA1 as CAN1 Tx
	GPIOPinConfigure(GPIO_PB5_CAN0TX);

	// Select the CAN function for these pins.
	GPIOPinTypeCAN( GPIO_PORTB_BASE, GPIO_PIN_5);
	GPIOPinTypeCAN( GPIO_PORTE_BASE, GPIO_PIN_4);

	// Initializes CAN1 Module, Clears memory objects
	CANInit(CAN0_BASE);

	// Set the bit rate to 250,000
	CANBitRateSet(CAN0_BASE, SysCtlClockGet(), CAN_BITRATE);

	CANIntClear(CAN0_BASE, 0xFFFFFFFF);
	CANIntRegister(CAN0_BASE, CAN_Handler);
	CANIntEnable(CAN0_BASE, CAN_INT_MASTER);

	// Enable the CAN bus
	CANEnable(CAN0_BASE);
	
	#ifdef CAN_MASTER
		 myaddress = 0;
		 LogMsg(CAN, MESSAGE, "Master Initialized");
	#endif

	#ifndef CAN_MASTER
		 //We have to request the bus master for an address
		 LogMsg(CAN, MESSAGE, "Not master, getting address");
		 FnReqAddress();
	#endif

	 // now we setup the receive message objects

	 //MO for functions
	 RxFunc_Msg.ui32MsgID = 0x0;
	 RxFunc_Msg.ui32MsgIDMask = 0x400; // we only need to match bit 10 for the functionality part
	 RxFunc_Msg.ui32Flags = (MSG_OBJ_RX_INT_ENABLE | MSG_OBJ_USE_ID_FILTER );
	 RxFunc_Msg.ui32MsgLen = 8;
	 CANMessageSet(CAN0_BASE, RX_FUNCTION, &RxFunc_Msg, MSG_OBJ_TYPE_RX);// Now we will receive an interrupt for all the functionality messages!

	 //MO for address messages sent to us
	 RxAddr_Msg.ui32MsgID = (0x1 << 10) | (myaddress << 5);
	 RxAddr_Msg.ui32MsgIDMask = (0x1 << 10) | (0x3E0); // Bit 10 set and our address in bits 5 - 9
	 RxAddr_Msg.ui32Flags = (MSG_OBJ_RX_INT_ENABLE | MSG_OBJ_USE_ID_FILTER );
	 RxAddr_Msg.ui32MsgLen = 8;
	 CANMessageSet(CAN0_BASE, RX_ADDRESS, &RxAddr_Msg, MSG_OBJ_TYPE_RX);// Now we will receive an interrupt for all the functionality messages!

	 //MO for address messages sent to everyone
	 RxAddr_Msg.ui32MsgID = (0x1 << 10) | (CAN_ADDRESS_ALL << 5);
	 RxAddr_Msg.ui32MsgIDMask = (0x1 << 10) | (0x3E0); // Bit 10 set and our address in bits 5 - 9
	 RxAddr_Msg.ui32Flags = (MSG_OBJ_RX_INT_ENABLE | MSG_OBJ_USE_ID_FILTER );
	 RxAddr_Msg.ui32MsgLen = 8;
	 CANMessageSet(CAN0_BASE, RX_ADDRESS_ALL, &RxAddr_Msg, MSG_OBJ_TYPE_RX);// Now we will receive an interrupt for all the functionality messages!

	 // must be called after an address is assigned and after MO are set
	 CAN_TunnelInit();
}

void CAN_Handler(void)
{
    uint32_t ui32Status;

    //
    // Read the CAN interrupt status to find the cause of the interrupt
    //
    ui32Status = CANIntStatus(CAN0_BASE, CAN_INT_STS_CAUSE);

    //
    // If the cause is a controller status interrupt, then get the status
    //
    if(ui32Status == CAN_INT_INTID_STATUS)
    {
        //
        // Read the controller status.  This will return a field of status
        // error bits that can indicate various errors.  Error processing
        // is not done in this example for simplicity.  Refer to the
        // API documentation for details about the error status bits.
        // The act of reading this status will clear the interrupt.  If the
        // CAN peripheral is not connected to a CAN bus with other CAN devices
        // present, then errors will occur and will be indicated in the
        // controller status.
        //
        ui32Status = CANStatusGet(CAN0_BASE, CAN_STS_CONTROL);

        //
        // Set a flag to indicate some errors may have occurred.
        //
        bErrFlag = 1;
    }

    //
    // Check if the cause is message object 2, which is used for sending
    // message 2.
    //
    else if(ui32Status == RX_FUNCTION)
    {
        //
        // Getting to this point means that the TX interrupt occurred on
        // message object 2, and the message TX is complete.  Clear the
        // message object interrupt.
        //
        CANIntClear(CAN0_BASE, RX_FUNCTION);
        //This is our receive message and should set the receive flag
        TaskQueueAdd(CAN_RxFunc_Handler,TASK_HIGH_PRIORITY);
    }
    else if( ui32Status == RX_ADDRESS)
    {
        CANIntClear(CAN0_BASE, RX_ADDRESS);
        TaskQueueAdd(CAN_RxAddr_Handler, TASK_HIGH_PRIORITY);

    }else if( ui32Status == RX_ADDRESS_ALL)
    {
        CANIntClear(CAN0_BASE, RX_ADDRESS_ALL );
        TaskQueueAdd(CAN_RxAddrAll_Handler, TASK_HIGH_PRIORITY);

    }
    else{
    	CANIntClear(CAN0_BASE, ui32Status);
    }
}



int8_t FnReqAddress(void)
{

	tCANMsgObject TxFunc_Msg;
	tCANMsgObject RxFunc_Msg;

	//Prepare the message object!
	int8_t i = 0, tmp = 0;
	bool bFail = 0; // flag to indicate failure of match
	int8_t rnd1 = (0x7F & random_int(0,127) );
	txData[0] = 0xFF; //identifier for an address request

	for(i = 1; i < 8;i++)
	{
		txData[i] = random_int(0,255);
	}
	TxFunc_Msg.ui32MsgID = rnd1; // The higher bits are zero and the lower ones we set !
	TxFunc_Msg.ui32MsgIDMask = 0;
	TxFunc_Msg.ui32Flags = 0;
	TxFunc_Msg.ui32MsgLen = sizeof(txData);
	TxFunc_Msg.pui8MsgData = txData;

	LogMsg(CAN, MESSAGE, "Requesting address, rand1 = %d, txData0 = %d, extended rand = %d, %d, %d, %d, %d, %d, %d. Waiting...", rnd1,
			txData[0], txData[1], txData[2], txData[3], txData[4], txData[5], txData[6], txData[7]);
	CANMessageSet(CAN0_BASE, TX_FUNCTION, &TxFunc_Msg, MSG_OBJ_TYPE_TX);

	// Now we wait for the reply, this has to be a blocking call since we cannot proceed with anything unless we have a valid address. Though a timeout should be implemented for error handling

	//our designated message for reception is MO 2.

    RxFunc_Msg.ui32MsgID = rnd1;
    RxFunc_Msg.ui32MsgIDMask = 0x780; // only the top three bits require a match
    RxFunc_Msg.ui32Flags = (MSG_OBJ_RX_INT_ENABLE | MSG_OBJ_USE_ID_FILTER ); // Use the provided filter and generate an interrupt upon the receipt of the message
    RxFunc_Msg.ui32MsgLen = 8; // The length of the data buffer that we expect

    CANMessageSet(CAN0_BASE, RX_FUNCTION, &RxFunc_Msg, MSG_OBJ_TYPE_RX);

    while(!checkMsgRxStatus(RX_FUNCTION)); // wait for the message to be received

    RxFunc_Msg.pui8MsgData = rxData;
    CANMessageGet(CAN0_BASE, RX_FUNCTION, &RxFunc_Msg, 0);


    //Now we can check if the bytes match and retrieve the address
    tmp = 0x7f & RxFunc_Msg.ui32MsgID;
    for(i = 1;i < 8;i++)
    {
    	if(txData[i] != rxData[i])
    	{
    		bFail = true;
    		break;
    	}
    }


    if(!bFail && (tmp == rnd1))
    {
    	myaddress = rxData[0];
    	LogMsg(CAN, MESSAGE, "Response received, address is: %d", myaddress);
    }
    else
    {
    	myaddress = 0xFF;
    	LogMsg(CAN, ERROR, "Response received, validation data did not match!");
    	return -1;
    }
    CANMessageClear(CAN0_BASE, RX_FUNCTION);

	return 0;// indicating success
}


void CAN_RxAddr_Handler(void){

	uint8_t RxAddr_Data[8];
	tCANMsgObject msg;

	uint8_t fromAddr = 0;

	msg.pui8MsgData = RxAddr_Data;

	CANMessageGet(CAN0_BASE, RX_ADDRESS , &msg, 0);

	//due to the masking we receive only the messages that pertain to us
	fromAddr =( msg.ui32MsgID & 0x1F); // the least five bits contain the address of the sender
	CAN_devices[fromAddr].last_msg = TimeNow();
	CAN_ParseAddr(fromAddr, &RxAddr_Data[0]);
}

void CAN_RxAddrAll_Handler(void){

	uint8_t RxAddr_Data[8];
	tCANMsgObject msg;

	uint8_t fromAddr = 0;

	msg.pui8MsgData = RxAddr_Data;

	CANMessageGet(CAN0_BASE, RX_ADDRESS_ALL , &msg, 0);

	//due to the masking we receive only the messages that are sent to everyone
	fromAddr =( msg.ui32MsgID & 0x1F); // the least five bits contain the address of the sender
	CAN_devices[fromAddr].last_msg = TimeNow();
	CAN_ParseAddr(fromAddr, &RxAddr_Data[0]);
}

void CAN_ParseAddr(uint8_t from, uint8_t * data) {
	union32_t sdata;
	uint8_t msg[8];
	switch(*data)
	{
	case TERM:
		// terminal tunnel message
		ReceiveTunnel(from, data+1);
		break;
	case COMMAND:
		// Command Message
		switch(*(data+1)) {
		case TURN_GREEN_LED_ON:
			CAN_GREEN_LED_ON();
			break;
		case TURN_GREEN_LED_OFF:
			CAN_GREEN_LED_OFF();
			break;
		case TURN_RED_LED_ON:
			CAN_RED_LED_ON();
			break;
		case TURN_RED_LED_OFF:
			CAN_RED_LED_OFF();
			break;
		case TURN_BLUE_LED_ON:
			CAN_BLUE_LED_ON();
			break;
		case TURN_BLUE_LED_OFF:
			CAN_BLUE_LED_OFF();
			break;
		case DEMO_LED:
			CAN_DEMO_LED();
			break;
		case RENAME:
			if(from == 0) {
				LogMsg(CAN, ERROR, "Duplicate name detected");
				CAN_Receiver(0);
			}
			break;
		default:
			LogMsg(CAN, WARNING, "Unknown command ID: %d", *(data+1));
			break;
		}
		//MsgCmd(fromAddr,msg4Data[1]);
		break;
	case NAME:
		// Name field
		// save the name for the sender
		CAN_SaveName(from, data+1);
		break;
	case EEPROM:
		// THis is an EEPROM Message
		if(*(data+1) == 1){ // Read the EEPROM and return

		}else {//write to EEPROM and get back with ACK

		}
		break;
	case SENSOR:
		switch(*(data+1)) {
		case SENSOR_RAW:
			sdata.b[0] = *(data+7);
			sdata.b[1] = *(data+6);
			sdata.b[2] = *(data+5);
			sdata.b[3] = *(data+4);
			LogMsg(CAN, MESSAGE, "Raw sensor data received, channel %d, value: %l", *(data+2), sdata.phrase);
			break;
		case SENSOR_GET_RAW:
			sdata.phrase = ADC_ReadChannel(*(data+2));
			msg[0] = SENSOR; msg[1] = SENSOR_RAW;
			msg[2] = *(data+2); msg[3] = 0;
			msg[4] = sdata.b[3]; msg[5] = sdata.b[2]; msg[6] = sdata.b[1]; msg[7] = sdata.b[0];
			LogMsg(CAN, MESSAGE, "Raw sensor data request received, sending channel %d, value: %l", *(data+2), sdata.phrase);
			CAN_TxAddressMode(from, &msg[0]);
			break;
		case SENSOR_INT_TEMP:
			sdata.b[0] = *(data+7);
			sdata.b[1] = *(data+6);
			sdata.b[2] = *(data+5);
			sdata.b[3] = *(data+4);
			LogMsg(CAN, MESSAGE, "Internal temperature data received, value: %l", sdata.phrase);
			break;
		case SENSOR_GET_INT_TEMP:
			sdata.phrase = ReadTemperature();
			msg[0] = SENSOR; msg[1] = SENSOR_INT_TEMP;
			msg[2] = 0; msg[3] = 0;
			msg[4] = sdata.b[3]; msg[5] = sdata.b[2]; msg[6] = sdata.b[1]; msg[7] = sdata.b[0];
			LogMsg(CAN, MESSAGE, "Internal temperature data request received, sending value: %l", sdata.phrase);
			CAN_TxAddressMode(from, &msg[0]);
			break;
		default:
			LogMsg(CAN, WARNING, "Unknown sensor ID: %d", *(data+1));
			break;
		}
		break;

	case SNAKE:

		switch(*(data+1)) {
			case REQUEST:
				// Prompt user to play snake
				LogMsg(CAN, MESSAGE, "Would you like to play snake? Press 'y' or 'Y' to join");
				//Create a receiver to get the response
				RegisterReceiverUART0(Snake_RequestReceiver);
				Snake_SetMaster(from);
				break;
			case JOIN:
				//Snake master should only get this
				if(Snake_GetStatus() == REQUESTING){
					// Add the player to the snake game
					Snake_AddPlayer(from);
					char * name;
					name = CAN_NameLookup(from);
					Push_printf(&tx0,"%s joined the game!\r\n", name);
				}
				break;

			case GAMEINFO:
				// Received by all playing snakes, should add these snakes to list
				// except for itself
				if(Snake_GetStatus() == WAITING){
					int i = 3;
					uint8_t addr = *(data+2);
					while( !(addr == 0xFF) && i < 8){
						Snake_AddPlayer(addr);
						addr = *(data+i);
						i++;
					}
				}
				break;
			case PLAY:
				if(Snake_GetStatus() == WAITING){
					PlaySnake();
				}
				break;
			case MOVE:
				if(Snake_GetStatus() == PLAYING){
					SnakeMove(from, (direction_t)*(data+2));
				}
				break;
			case FRUIT:
				if(Snake_GetStatus() == PLAYING){
					SetFruit(*(data+2), *(data+3));
					PushFruit();
				}
				break;
			case UPDATE:
				if(Snake_GetStatus() == PLAYING){
					Snake_UpdateGame();
				}
				break;
			case GAMEOVER:
				if(Snake_GetStatus() == PLAYING){
					Snake_GameOver();
				}
				break;
			default:
				break;
		}

		break;


	default:
		LogMsg(CAN, WARNING, "Unknown message type: %d", *data);
		break;
	}
}

void CAN_RxFunc_Handler(void){

	tCANMsgObject TxFunc_Msg;
	tCANMsgObject RxFunc_Msg;
	uint8_t rxData2[8];
	int i;

	RxFunc_Msg.pui8MsgData = rxData2;
	CANMessageGet(CAN0_BASE,2,&RxFunc_Msg,0);
	LogMsg(CAN, VERBOSE_MESSAGE, "FuncMsgRx, ID: 0x%x, length: %d, data: %d, %d, %d, %d, %d, %d, %d, %d",
			RxFunc_Msg.ui32MsgID, RxFunc_Msg.ui32MsgLen, rxData2[0], rxData2[1], rxData2[2], rxData2[3],
			rxData2[4], rxData2[5], rxData2[6], rxData2[7]);
#ifdef CAN_MASTER
	if((RxFunc_Msg.ui32MsgID & 0x780) == 0) // Check if bit 10-7 are zero bits and data[0] is 0xFF
	{
		if(rxData2[0] == 0xFF)
		{
			// An address is requested
			// Prepare the response object
			TxFunc_Msg.ui32MsgID = RxFunc_Msg.ui32MsgID;
			TxFunc_Msg.ui32MsgIDMask = 0;
			TxFunc_Msg.ui32Flags = 0;
			TxFunc_Msg.ui32MsgLen = 8;
			for(i = 1;i < 8;i++)//we send back the same data as was received
				txData[i] = rxData2[i];
			addrCount++;
			if(addrCount >= CAN_MAX_DEVICES-1) {
				addrCount = CAN_MAX_DEVICES;
				txData[0] = CAN_GetObsoleteAddress();
				LogMsg(CAN, MESSAGE, "Valid address request received, out of addresses, address assigned: %d, last used %d seconds ago",
						txData[0], TimeSince(CAN_devices[txData[0]].last_msg)/1000);
				// reset the last message time to now so this address doesn't get used again right away
				CAN_devices[txData[0]].last_msg = TimeNow();
			}else {
				txData[0] = addrCount;
				LogMsg(CAN, MESSAGE, "Valid address request received, address assigned: %d", txData[0]);
			}
			TxFunc_Msg.pui8MsgData = txData;

			CANMessageSet(CAN0_BASE, TX_FUNCTION, &TxFunc_Msg, MSG_OBJ_TYPE_TX);
			return; // All Done
		}else LogMsg(CAN, ERROR, "Invalid address request, data0 = %d", rxData2[0]);
	}else LogMsg(CAN, WARNING, "Unknown function message received");
#else
	if((RxFunc_Msg.ui32MsgID & 0x780) == 0) // Check if bit 10-7 are zero bits and data[0] is 0xFF
	{
		LogMsg(CAN, VERBOSE_MESSAGE, "Address request received, not responding");
	}else LogMsg(CAN, WARNING, "Unknown function message received");
#endif

}

uint8_t CAN_TxAddressMode(uint8_t address, uint8_t * data){

	tCANMsgObject TxMsg;

	TxMsg.ui32MsgID = (1<<10) | (address << 5) | myaddress;
	TxMsg.ui32MsgIDMask = 0;
	TxMsg.ui32Flags = 0;
	TxMsg.ui32MsgLen = 8;
	TxMsg.pui8MsgData = data;
	CANMessageSet(CAN0_BASE, TX_ADDRESS, &TxMsg, MSG_OBJ_TYPE_TX);

	tint_t txTime = TimeNow();
	while(checkMsgTxStatus(TX_FUNCTION)){
		if(TimeSince(txTime) >= CAN_TX_TIMEOUT_MS) {
			// Log a sad face for paterson
			LogMsg(CAN, ERROR, ":(");
			break;
		}
	}
	return 1;

}

uint8_t CAN_TxFunctionMode(uint16_t id, uint8_t * data){

	tCANMsgObject TxMsg;

	TxMsg.ui32MsgID = id;
	TxMsg.ui32MsgIDMask = 0;
	TxMsg.ui32Flags = 0;
	TxMsg.ui32MsgLen = 8;
	TxMsg.pui8MsgData = data;
	CANMessageSet(CAN0_BASE, TX_FUNCTION, &TxMsg, MSG_OBJ_TYPE_TX);

	tint_t txTime = TimeNow();
	while(checkMsgTxStatus(TX_FUNCTION)){
		if(TimeSince(txTime) >= CAN_TX_TIMEOUT_MS) {
			// Log a sad face for paterson
			LogMsg(CAN, ERROR, ":(");
			break;
		}
	}

	return 1;
}


uint8_t checkMsgRxStatus(uint8_t MsgNum){

	return ( CANStatusGet(CAN0_BASE, CAN_STS_NEWDAT) & (1 << (MsgNum - 1)) );

}

uint8_t checkMsgTxStatus(uint8_t MsgNum){

	return ( CANStatusGet(CAN0_BASE, CAN_STS_TXREQUEST) & (1 << (MsgNum - 1)) );

}

uint8_t CAN_GetMessageObjectID(void) {
	uint8_t id;
	for(id = UNUSED_CAN_MO; id <= 31; id++) {
		if(message_objects_used & (0x00000001 << id) == 0) break;
	}
	if(id > 31) return 0xFF;
	return id;
}

void CAN_FreeMessageObjectID(uint8_t id) {
	message_objects_used &= ~(0x00000001 << id);
}

void CAN_Callback(char * str) {
	uint8_t i = 0;
	// create flags
	enum cmd c = BASE;
	uint8_t a = 0xFF;
	uint8_t s = 0xFF;

	uint8_t data[8] = {0, 0, 0, 0, 0, 0, 0, 0};

	while (*(str+i) != RECEIVE_STOP_CHAR) {
		// if the index is greater than length then break;
		if (i >= RECEIVE_MAX_LENGTH-4) break;
		// check for flag character '-' and increment i
		if (*(str+i++) == RECEIVE_FLAG_CHAR) {
			// if a flag switch the flag character and process the value if needed
			switch (*(str+i)) {
				case 'c':
				case 'C':
					c = (enum cmd)FlagStr2Int((str + ++i));
					break;
				case 'a':
				case 'A':
					a = FlagStr2Int((str + ++i));
					break;
				case 's':
				case 'S':
					s = FlagStr2Int((str + ++i));
					break;
				default:
					LogMsg(CAN, WARNING, "Unknown flag");
					break;
			}
		}
	}

	if(c > BASE && c < LAST_CMD) {
		if(a != 0xFF) {
			data[0] = COMMAND;
			data[1] = c;
			LogMsg(CAN, MESSAGE, "Sending command ID: %d, to address: %d", c, a);
			CAN_TxAddressMode(a, &data[0]);
		}else LogMsg(CAN, WARNING, "Invalid address");
	}else if(c != BASE) LogMsg(CAN, WARNING, "Unknown command ID: %d", c);

	if(s != 0xFF) {
		if(s <= ADC_LAST_CHANNEL) {
			data[0] = SENSOR;
			data[1] = SENSOR_GET_RAW;
			data[2] = s;
			LogMsg(CAN, MESSAGE, "Sending raw sensor data request for channel %d to address: %d", s, a);
			CAN_TxAddressMode(a, &data[0]);
		}else if(s == ADC_INTERNAL_TEMPERATURE_CHANNEL) {
			data[0] = SENSOR;
			data[1] = SENSOR_GET_INT_TEMP;
			LogMsg(CAN, MESSAGE, "Sending internal temperature request to address: %d", a);
			CAN_TxAddressMode(a, &data[0]);
		}
	}

	if (strncmp(str, "dev", 3) == 0) {
		CAN_PrintDeviceList();
	}

	if (strncmp(str, "snake", 5) == 0) {
		// Send message to everyone to request snake players
		data[0] = SNAKE;
		data[1] = REQUEST;
		LogMsg(CAN, MESSAGE, "You are the snake master! When you want to begin, send $SNAKE p.  Sending snake request. ");
		CAN_TxAddressMode(CAN_ADDRESS_ALL, &data[0]);

		//Initialize this snake as master
		Snake_MasterInit();
	}
}
