/*
 * CAN_cmds.c
 *
 *  Created on: Apr 16, 2014
 *      Author: Rob
 */

#include "CAN_cmds.h"
#include "buffer.h"
#include "buffer_printf.h"
#include <string.h>

#define FRAME_LENGTH 8

#define CAN_START_CHAR '#'

#ifndef BACKSPACE
#define BACKSPACE 8
#endif

struct CAN_device_t CAN_devices[CAN_MAX_DEVICES];

enum eCAN_states {RECEIVING_NAME, WAITING_FOR_START, RECEIVING_ADDRESS, RECEIVING_NUM_ADDRESS, RECEIVING_CHAR_ADDRESS, TRANSMITTING};

void CAN_Receiver(char c);
void CAN_SendMyName(void);

void CAN_TunnelInit(void) {
	uint8_t i, j;
	CAN_RegisterReceiver(CAN_Receiver);
	CAN_Receiver(0);
	for(i = 0; i < CAN_MAX_DEVICES; i++) {
		for(j = 0; j < 7; j++) {
			CAN_devices[i].name[j] = 0;
		}
		CAN_devices[i].last_msg = 0;
	}
	CAN_devices[CAN_MAX_DEVICES-1].address = CAN_MAX_DEVICES-1;
	CAN_devices[CAN_MAX_DEVICES-1].name[0] = 'a';
	CAN_devices[CAN_MAX_DEVICES-1].name[1] = 'l';
	CAN_devices[CAN_MAX_DEVICES-1].name[2] = 'l';
}

void CAN_Receiver(char c) {
	static enum eCAN_states state;
	static uint8_t index = 0;
	static uint8_t buffer[8];
	static uint8_t address;
	uint8_t i;
	if(c == 0) {
		state = RECEIVING_NAME;
		LogStr("\r\n Enter 1-6 character name for your node and press enter: ");
		LogEchoOn();
		index = 0;
		return;
	}
	switch(state) {
	case RECEIVING_NAME:
		// if backspace then decrement index
		if(c == BACKSPACE) {
			// erase char
			CAN_devices[CAN_GetMyAddress()].name[index] = 0;
			if(index) index--;
			return;
		}
		if((c == '\r' && index > 0) || index >= 6) {
			if(index) {
				// ensure string is null terminated
				CAN_devices[CAN_GetMyAddress()].name[index] = 0;
				LogStr("\n");
				LogMsg(CAN, IMPORTANT_MESSAGE, "Name saved: %s", &CAN_devices[CAN_GetMyAddress()].name[0]);
				CAN_SendMyName();
				// resend name every CAN_SEND_NAME_PERIOD ms
				TaskScheduleAdd(CAN_SendMyName, TASK_LOW_PRIORITY, CAN_SEND_NAME_PERIOD, CAN_SEND_NAME_PERIOD);
				state = WAITING_FOR_START;
			}else {
				LogMsg(CAN, WARNING, "0 characters entered., try again");
			}
			return;
		}
		CAN_devices[CAN_GetMyAddress()].name[index++] = c;
		break;
	case WAITING_FOR_START:
		if(c == CAN_START_CHAR) {
			state = RECEIVING_ADDRESS;
			index = 0;
		}
		break;
	case RECEIVING_ADDRESS:
		if(c == BACKSPACE) {
			state = WAITING_FOR_START;
			return;
		}
		buffer[index++] = c;
		if(c >= '0' && c <= '9') {
			state = RECEIVING_NUM_ADDRESS;
			address = c - '0';
		}else state = RECEIVING_CHAR_ADDRESS;
		break;
	case RECEIVING_NUM_ADDRESS:
		if(c == BACKSPACE) {
			address /= 10;
		}else if(c >= '0' && c <= '9') {
			address *= 10;
			address += c - '0';
		}else if(c == ' ') {
			if(address < CAN_MAX_DEVICES) {
				state = TRANSMITTING;
				buffer[0] = TERM;
				index = 1;
			}else {
				LogMsg(CAN, WARNING, "Address out of range");
				state = WAITING_FOR_START;
			}
		}else {
			LogMsg(CAN, WARNING, "Invalid numeric address format");
			state = WAITING_FOR_START;
		}
		break;
	case RECEIVING_CHAR_ADDRESS:
		if(c == BACKSPACE && index) { index--; break; }
		buffer[index++] = c;
		if(c == ' ') {
			address = CAN_AddrLookup(&buffer[0]);
			if(address >= CAN_MAX_DEVICES) {
				LogMsg(CAN, VERBOSE_MESSAGE, "Address not found given entered name");
				state = WAITING_FOR_START;
			}else {
				state = TRANSMITTING;
				buffer[0] = TERM;
				index = 1;
			}
		}else if(index > 6) {
			LogMsg(CAN, WARNING, "Name too long");
			state = WAITING_FOR_START;
		}
		break;
	case TRANSMITTING:
		if(c == '\r') {
			LogStr("\n");
			for(i = index; i < 8; i++) {
				buffer[i] = 0;
			}
			CAN_TxAddressMode(address, &buffer[0]);
			state = WAITING_FOR_START;
		}else {
			buffer[index++] = c;
			if(index >= 8) {
				CAN_TxAddressMode(address, &buffer[0]);
				index = 1;
			}
		}
		break;
	}
}

/*

uint8_t CAN_TxAddressMode(uint8_t address, uint8_t *data);

void TunnelMsg(char *msg) {
	uint8_t i, j, addr = 0, frame_data_counter = 0;
	uint8_t frame_data[FRAME_LENGTH] = {0, 0, 0, 0, 0, 0, 0, 0};
	

	for (i = 0; i < strlen(msg); i++) {
		if (*(msg + i) >= '0' && *(msg + i) <= '9') {
			addr = addr * 10 + (*(msg + i) - '0');
		} else if (*(msg + i) == ' ') {
			break;
		} else {
			return; // bad address
		}
	}

	for (i = i + 1; i < strlen(msg); i++) {
		frame_data[++frame_data_counter] = *(msg + i);
		if (frame_data_counter == FRAME_LENGTH - 1) { // we wrote the last char to the frame
			CAN_TxAddressMode(addr, frame_data);
			frame_data_counter = 0;
			
			// clear frame data
			for (j = 0; j < FRAME_LENGTH; j++) {
				frame_data[j] = 0;
			}
		}
	}

	if (frame_data_counter > 0) {
		CAN_TxAddressMode(addr, frame_data);
	}

}
*/

void ReceiveTunnel(uint8_t address, uint8_t *data) {
	static uint8_t last_address = 0xFF;
	uint8_t i;
	if(address != last_address) {
		// if name exists then print that, otherwise print the address number
		if(CAN_devices[address].name[0]) LogStr("%s: ", &CAN_devices[address].name[0]);
		else LogStr("From %d: ", address);
		last_address = address;
	}
	for(i = 0; i < 7; i++) {
		if(*(data+i) == 0) {
			Push(LOG_BUF, '\r');
			Push(LOG_BUF, '\n');
			last_address = 0xFF; return;
		}
		Push(LOG_BUF, *(data+i));
	}
}

uint8_t CAN_AddrLookup(uint8_t * str) {
	uint8_t match;
	uint8_t index;
	uint8_t device;
	uint8_t address;
	for(index = 0; index < 6; index++) {
		match = 0;
		for(device = 0; device < CAN_MAX_DEVICES; device++) {
			if(CAN_devices[device].name[index] == *(str+index)) {
				match++;
				address = device;
			}
		}
		// partial match complete
		if(match == 1) return address;
	}
	if(match) {
		LogMsg(CAN, WARNING, "Duplicate names found");
		return address;
	}
	return 0xFF;
}

char * CAN_NameLookup(uint8_t addr) {
	return &CAN_devices[addr].name[0];
}

void CAN_SendMyName(void) {
	uint8_t data[8];
	uint8_t i;
	uint8_t address;
	address = CAN_GetMyAddress();
	data[0] = NAME;
	for(i = 0; i < 6; i++) {
		data[i+1] = CAN_devices[address].name[i];
	}
	data[7] = 0;
	LogMsg(CAN, VERBOSE_MESSAGE, "Name sent: %s", &data[1]);
	CAN_TxAddressMode(CAN_ADDRESS_ALL, &data[0]);
}

void CAN_SaveName(uint8_t from, uint8_t *name) {
	uint8_t i;
	if(from >= CAN_MAX_DEVICES) {
		LogMsg(CAN, WARNING, "Invalid from address while saving name: %d: ", from);
		return;
	}
#ifdef CAN_MASTER
	uint8_t j;
	uint8_t msg[8];
	// search for duplicate names
	for(i == 0; i < CAN_MAX_DEVICES; i++) {
		// ignore the current name for  the device
		if(i == from) continue;
		// if a duplicate is found then reject the name
		if(strncmp(name, CAN_devices[i].name, 6) == 0) {
			msg[0] = COMMAND;
			msg[1] = RENAME;
			msg[2] = 0;
			CAN_TxAddressMode(from, &msg[0]);
			LogMsg(CAN, ERROR, "Duplicate names detected, rename requested");
			return;
		}
	}
#endif
	if(from >= CAN_MAX_DEVICES) {
		LogMsg(CAN, WARNING, "Invalid from address while saving name: %d: ", from);
		return;
	}
	for(i = 0; i < 6; i++) {
		CAN_devices[from].name[i] = *name++;
	}
}

uint8_t CAN_GetObsoleteAddress(void) {
	uint8_t address, i;
	tint_t oldest = 0;
	// find the address who has the oldest last message received
	// start at i = 1 so you don't accidently give away the master address
	for(i = 1; i < CAN_MAX_DEVICES - 1; i++) {
		if(TimeSince(CAN_devices[i].last_msg) > oldest) {
			address = i;
			oldest = TimeSince(CAN_devices[i].last_msg);
		}
	}
	return address;
}

void CAN_PrintDeviceList(void) {
	uint8_t i;
	LogStr("Addr\tName\tLastMsgRx\r\n");
	for(i = 0; i < CAN_MAX_DEVICES; i++) {
		LogStr("%d\t%s\t%d\r\n", i, &CAN_devices[i].name[0], TimeSinceSec(CAN_devices[i].last_msg));
		while(GetSize(LOG_BUF));
	}
}
