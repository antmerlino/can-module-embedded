/**
 *  This module receives a data value from the user. This value is the "Channel" that
 *  the ADC will operate at. The function sets up the appropriate channel for the user.
 */


#include <stdint.h>
#include <stdbool.h>
#include "driverlib/adc.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "ADC_ReadChannel.h"


uint32_t ADC_ReadChannel(uint8_t msg4Data){

	uint32_t ADC_DATA; // declare variable to be returned

	//ENABLE ADC0
	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	
	//ENABLE periph for GPIO B,D, and E
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	
	//Disable ADC prior to usage. Reduces the risk of unwanted data
	ADCSequenceDisable(ADC0_BASE, 3);
	
	//Use the 3rd Sample sequencer. This means only 1 sample being taken
	//and depth of FIFO is 1. highest priority
	ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
	

	switch(msg4Data){
		case 0:
			GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_3);
			ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH0 | ADC_CTL_IE | ADC_CTL_END);
			break;
		case 1:
			GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_2); 
			ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH1 | ADC_CTL_IE | ADC_CTL_END);
			break;
		case 2:
			GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_1); 
			ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH2 | ADC_CTL_IE | ADC_CTL_END);
			break;
		case 3:
			GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_0); 
			ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH3 | ADC_CTL_IE | ADC_CTL_END); 
			break;
		case 4:
			GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_3); 
			ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH4 | ADC_CTL_IE | ADC_CTL_END); 
			break;
		case 5:
			GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_2); 
			ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH5 | ADC_CTL_IE | ADC_CTL_END); 
			break;
		case 6:
			GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_1); 
			ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH6 | ADC_CTL_IE | ADC_CTL_END); 
			break;
		case 7:
			GPIOPinTypeADC(GPIO_PORTD_BASE, GPIO_PIN_0); 
			ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH7 | ADC_CTL_IE | ADC_CTL_END); 
			break;
		case 8:
			GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_5);
			ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH8 | ADC_CTL_IE | ADC_CTL_END); 
			break;
		case 9:
			GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_4); 
			ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH9 | ADC_CTL_IE | ADC_CTL_END); 
			break;
		case 10:
			GPIOPinTypeADC(GPIO_PORTB_BASE, GPIO_PIN_4);
			ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH10 | ADC_CTL_IE | ADC_CTL_END);
			break;
		case 11:
			GPIOPinTypeADC(GPIO_PORTB_BASE, GPIO_PIN_5); 
			ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH11 | ADC_CTL_IE | ADC_CTL_END);
			break;
	}

	//Enable the ADC
	ADCSequenceEnable(ADC0_BASE, 3);
	
	//Clear interrupt to proceed to data capture
	ADCIntClear(ADC0_BASE, 3);
	
	//ask processor to trigger ADC
	ADCProcessorTrigger(ADC0_BASE, 3);

	while (!ADCIntStatus(ADC0_BASE, 3, false)){} //Do nothing until interrupt is triggered

	ADCIntClear(ADC0_BASE, 3); //Clear Interrupt to proceed to next data capture
	//read value of level
	ADCSequenceDataGet(ADC0_BASE, 3, &ADC_DATA);

	return ADC_DATA;
		
}
