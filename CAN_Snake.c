/*
 * MER_game1.c
 *
 *  Created on: Apr 11, 2014
 *      Author: Anthony
 */

#include "subsys.h"
#include "buffer_printf.h"
#include "random_int.h"
#include "task.h"
#include "cansys.h"
#include "timing.h"
#include "CAN_Snake.h"
#include "CAN_cmds.h"

#define SNAKE_VERSION 0x01010001u

#define ESC 0x1B
#define RED 31
#define BLUE 34
#define GREEN 32

#define MAX_SNAKES CAN_MAX_DEVICES-1

#define SNAKE_SPEED 100

typedef struct{
	void (*registerFunction)();
	void (*unregisterFunction)();
	buffer_t *txBuf;
	receiver_t receiver;
	uint8_t color;

}player_config_t;

typedef struct{
	uint16_t width;
	uint16_t height;
}board_t;


typedef struct{
	uint8_t x[100];
	uint8_t y[100];
	direction_t nextDirection;
	direction_t direction;
	uint16_t size;
	uint8_t can_addr;
}snake_t;

typedef struct{
	uint8_t x;
	uint8_t y;
} fruit_t;

snake_t snakes[MAX_SNAKES];
uint8_t numSnakes = 0;
fruit_t fruit;
uint8_t snakeMaster;
board_t board;
game_status_t gameStatus = UINITIALIZED;
uint8_t mySnakeIndex;

// note the user doesn't need to access these functions directly so they are
// defined here instead of in the .h file
// further they are made static so that no other files can access them
// (also to avoid errors if the same function name is used in another file)

static void Snake_Receiver(char c);

static void Snake_Callback(char * cmd);

static void PushCharXY(buffer_t * buf, char c, char x, char y);
static void shift(uint8_t *a, uint16_t n);
static void FruitCollision(snake_t *snake);
static void SnakeCollision(snake_t *snake2);
static void SelfCollision(snake_t *snake);
static void UpdateSnake(snake_t *snake);
static void UpdateFruit(void);
static void Snake_Remove(void);

void Snake_SetMaster(uint8_t addr){snakeMaster = addr;}
game_status_t Snake_GetStatus(void){return gameStatus;}
void Snake_AddPlayer(uint8_t addr){snakes[numSnakes++].can_addr = addr;}


void Snake_MasterInit(void){

	version_t v;
	v.word = SNAKE_VERSION;
	// Register the module with the system and give it the name "SNAKE"
	SubsystemInit(SNAKEGAME, MESSAGE, "SNAKE", v);

	RegisterCallback(SNAKEGAME, Snake_Callback);
	Snake_SetMaster(CAN_GetMyAddress());
	// There is just the master at the start
	snakes[0].can_addr = snakeMaster;
	numSnakes = 1;
	gameStatus = REQUESTING;
}

void Snake_Callback(char * cmd) {

	uint8_t data[8] = {0, 0, 0, 0, 0, 0, 0, 0};

	// process the "SNAKE" commands
	// since we only implement "play", "help", and "reset" we can just test the first char
	// of the command string to determine if the user pressed 'p' or 'h' or 'r'
	if(*cmd == 'p') {

		int i = 0;
		int j;

		while(i < numSnakes){
			//Send message to everyone and provide number of players and addr of each player
			data[0] = SNAKE;
			data[1] = GAMEINFO;
			for(j = 0; j < 6; j++){
				if(j+i < numSnakes){
					data[j+2] = snakes[j+i].can_addr;
				}
				else{
					data[j+2] = 0xFF;
				}
			}
			CAN_TxAddressMode(CAN_ADDRESS_ALL, &data[0]);

			i += 6;
		}

		data[0] = SNAKE;
		data[1] = PLAY;
		CAN_TxAddressMode(CAN_ADDRESS_ALL, &data[0]);

		// Schedule Task to print fruit in 0.5s
		TaskScheduleAdd(UpdateFruit, TASK_HIGH_PRIORITY, 500, 0);

		// Schedule UpdateGame task to start in 1s and occur every SNAKE_SPEED
		TaskScheduleAdd(Snake_UpdateGame, TASK_HIGH_PRIORITY, 1000, SNAKE_SPEED);

		PlaySnake();
	}
}

void calculateBoardSize(board_t * brd, uint8_t numSnakes){
//	brd->width = 2*(numSnakes*16);
//	brd->height = (numSnakes*16);
	brd->width = 120;
	brd->height = 40;
}

void SnakeMove(uint8_t addr, direction_t direction){
	int i;
	for(i = 0; i < numSnakes; i++){
		if(addr == snakes[i].can_addr){
			snakes[i].direction = direction;
			snakes[i].nextDirection = direction;
			break;
		}
	}
}

void PlaySnake(void) {

	gameStatus = PLAYING;
	LogEchoOff();

	calculateBoardSize(&board, numSnakes);

	char x,y;
	// clear the screen
	Push_printf(&tx0,"\f");

	// draw a box around our map
	PushCharXY(&tx0,201,0,0);

	for(x=1; x < board.width; x++) PushCharXY(&tx0,205,x,0);

	PushCharXY(&tx0,187,board.width,0);

	for(y=1; y<board.height; y++) PushCharXY(&tx0,186,board.width,y);

	// wait to transmit all the chars before continuing
	// (this could be removed if the TX buffer size is increased)
	// another way around this would be to schedule the rest of the printout
	// to happen several ms from now
	while(GetSize(&tx0));

	PushCharXY(&tx0,188,board.width,board.height);

	for(x=1; x<board.width; x++) PushCharXY(&tx0,205,x,board.height);

	PushCharXY(&tx0,200,0,board.height);

	for(y=1; y<board.height; y++) PushCharXY(&tx0,186,0,y);

	while(GetSize(&tx0));

	int i;
	// Initialize Snakes
	for(i = 0; i < numSnakes; i++){

		snakes[i].size = 1;

		if(!i%2){
			snakes[i].x[0] = board.width/2 + 6*(i) + 6;
		}
		else{
			snakes[i].x[0] = board.width/2 - 6*(i-1) - 6;
		}

		snakes[i].y[0] = board.height/2;
		snakes[i].direction = RESET;
		snakes[i].nextDirection = RESET;

		if(snakes[i].can_addr == CAN_GetMyAddress()){
			mySnakeIndex = i;
		}

		// Print the snake
		PushCharXY(&tx0, 254, snakes[i].x[0], snakes[i].y[0]);

	}

	//Register Receiver
	RegisterReceiverUART0(Snake_Receiver);

	// hide cursor
	Push_printf(&tx0,"%c[?25l",ESC);

	// set cursor below bottom of map
	PushCharXY(&tx0,'\r',0,board.height+1);

}


void Snake_Receiver(char c) {

	switch(c) {
		case 'w':
		case 'W':
			// If we try to move up while moving down don't allow it
			if(snakes[mySnakeIndex].direction != DOWN){
				snakes[mySnakeIndex].nextDirection = UP;
			}
			break;
		case 's':
		case 'S':
			// If we try to move down while moving up don't allow it
			if(snakes[mySnakeIndex].direction != UP){
				snakes[mySnakeIndex].nextDirection = DOWN;
			}
			break;
		case 'a':
		case 'A':
			// If we try to move left while moving right don't allow it
			if(snakes[mySnakeIndex].direction != RIGHT){
				snakes[mySnakeIndex].nextDirection = LEFT;
			}
			break;
		case 'd':
		case 'D':
			// If we try to move right while moving left don't allow it
			if(snakes[mySnakeIndex].direction != LEFT){
				snakes[mySnakeIndex].nextDirection = RIGHT;
			}
			break;
		case ESC:
			Snake_Remove();
			break;

		default:
			break;
	}
}


void Snake_RequestReceiver(char c){

	uint8_t data[8] = {0, 0, 0, 0, 0, 0, 0, 0};

	switch(c){
		case 'y':
		case 'Y':
			// Respond to the snake master letting them know we want to play
			data[0] = SNAKE;
			data[1] = JOIN;
			LogMsg(CAN, MESSAGE, "You are joining the snake game. Waiting for snake master to start game... ");
			CAN_TxAddressMode(snakeMaster, &data[0]);
			numSnakes = 0;
			gameStatus = WAITING;

			break;
		default:
			gameStatus = UINITIALIZED;
			break;

	}
	// We only need the receiver while the user responds
	UnregisterReceiverUART0(Snake_RequestReceiver);
}

void Snake_UpdateGame(void){

	uint8_t data[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	int i, j;

	//If your the master, tell everyone to update
	if(snakes[mySnakeIndex].can_addr == snakeMaster){
		data[0] = SNAKE;
		data[1] = UPDATE;
		CAN_TxAddressMode(CAN_ADDRESS_ALL, &data[0]);

		uint8_t dead_snakes = 0;
		//Check if all snakes are dead
		for(i = 0; i < numSnakes; i++){
			//If any snake is not dead, break
			if(snakes[i].direction == DEAD){
				dead_snakes++;
			}
		}

		if(dead_snakes == numSnakes){
			//If we've made it to here, all snakes are dead

			//Send the Gameover message
			data[0] = SNAKE;
			data[1] = GAMEOVER;
			CAN_TxAddressMode(CAN_ADDRESS_ALL, &data[0]);

			Snake_GameOver();

			//Remove the update task
			RemoveTask(Snake_UpdateGame);
		}

	}

	uint16_t temp_size;

	// Update the snakes and check for collisions with other things
	for(i = 0; i < numSnakes; i++){

		//If the snake is not in dead, update and check the snake
		if(snakes[i].direction != DEAD){
			UpdateSnake(&snakes[i]);

			SelfCollision(&snakes[mySnakeIndex]);

			temp_size = snakes[i].size;

			FruitCollision(&snakes[i]);

			// If the size is the same, we didn't hit a fruit, so let's delete our tail
			if(temp_size == snakes[i].size){
					PushCharXY(&tx0, ' ', snakes[i].x[snakes[i].size], snakes[i].y[snakes[i].size]);
			}
		}
	}

	// We need to check if our snake collided with any other snake
	for (i = 0; i < numSnakes; i++){
		if(i != mySnakeIndex){
			//Only check against snakes that are not dead
			if(snakes[i].direction != DEAD){
				SnakeCollision(&snakes[i]);
			}
		}
	}

	//If any of the snakes are dead and this is the first time we see that, we should remove them from the screen
	for (i = 0; i < numSnakes; i++){
		if(snakes[i].nextDirection == DEAD){
			for(j = 0; j < snakes[i].size; j++){
				PushCharXY(&tx0, ' ', snakes[i].x[j], snakes[i].y[j]);
			}
		}
	}

	// Step through snakes to print
	for(i = 0; i < numSnakes; i++){
		//Only print snakes that aren't dead
		if(snakes[i].direction != DEAD){
			//Set terminal color to snake color
			if(i == mySnakeIndex){
				Push_printf(&tx0,"%c[%dm",ESC, RED);
			}
			// Print the snake
			PushCharXY(&tx0, 254, snakes[i].x[0], snakes[i].y[0]);
			// Reset color
			Push_printf(&tx0,"%c[%dm",ESC, 0);
		}
	}

	//Now that we are complete the game update, send our updated direction if it has changed
	//We want to send when we complete the game loop so that we only are sending messages at the
	//beginning of the time step.  This way messages should not conflict with the update message
	if(snakes[mySnakeIndex].direction != snakes[mySnakeIndex].nextDirection){
		data[0] = SNAKE;
		data[1] = MOVE;
		data[2] = snakes[mySnakeIndex].nextDirection;
		CAN_TxAddressMode(CAN_ADDRESS_ALL, &data[0]);
		snakes[mySnakeIndex].direction = snakes[mySnakeIndex].nextDirection;
	}

	// set cursor below bottom of map
	PushCharXY(&tx0,'\r',0,board.height+1);
}

void UpdateSnake(snake_t *snake){

	switch(snake->direction){

		case RESET:

			break;

		case UP:

			shift(&snake->x[0], snake->size);
			shift(&snake->y[0], snake->size);

			snake->y[0] = snake->y[1] - 1;

			if(snake->y[0] <= 0){
				snake->y[0] = board.height - 1;
			}

			break;

		case DOWN:

			shift(&snake->x[0], snake->size);
			shift(&snake->y[0], snake->size);

			snake->y[0] = snake->y[1] + 1;

			if(snake->y[0] >= board.height){
				snake->y[0] = 1;
			}

			break;

		case LEFT:

			shift(&snake->x[0], snake->size);
			shift(&snake->y[0], snake->size);

			snake->x[0] = snake->x[1] - 2;

			if(snake->x[0] <= 0){
				snake->x[0] = board.width - 2;
			}

			break;

		case RIGHT:

			shift(&snake->x[0], snake->size);
			shift(&snake->y[0], snake->size);

			snake->x[0] = snake->x[1] + 2;

			if(snake->x[0] >= board.width){
				snake->x[0] = 2;
			}

			break;

		default:
			break;
	}


}

void PushCharXY(buffer_t * buf, char c, char x, char y) {
	Push_printf(buf,"%c[%d;%dH%c",ESC,y+1,x+1,c);
}

void shift(uint8_t * a, uint16_t n) {
   int i;
   for(i = n; i > 0; i--){
      *(a+i) = *(a+i-1);
   }
}


void PopulateFruit(){

	uint8_t validPosition = 0;

	while(!validPosition){

		fruit.x = random_int(1, board.width - 2);

		if(fruit.x % 2){
			fruit.x += 1;
		}

		fruit.y = random_int(1, board.height - 1);

		validPosition = 1;

		int i, j;
		//Step through all the snakes
		for(i = 0; i < numSnakes; i++){
			for(j = 0; j < snakes[i].size; j++){
				if(fruit.x == snakes[i].x[j] && fruit.y == snakes[i].y[j]){
					validPosition = 0;
					break;
				}
			}
			if(!validPosition){
				break;
			}
		}

	}
}

void SetFruit(uint8_t x, uint8_t y){
	fruit.x = x;
	fruit.y = y;
}

void PushFruit(void){
	//Set terminal color to snake color
	Push_printf(&tx0,"%c[%dm",ESC, GREEN);
	PushCharXY(&tx0, 161, fruit.x, fruit.y);
	Push_printf(&tx0,"%c[%dm",ESC, 0);
}


void UpdateFruit(){

	uint8_t data[8] = {0, 0, 0, 0, 0, 0, 0, 0};

	PopulateFruit();
	PushFruit();

	// Send CAN message to let others no the location of the fruit
	data[0] = SNAKE;
	data[1] = FRUIT;
	data[2] = fruit.x;
	data[3] = fruit.y;
	CAN_TxAddressMode(CAN_ADDRESS_ALL, &data[0]);
}

void FruitCollision(snake_t * snake){


	// If we hit a piece of fruit
	if(snake->x[0] == fruit.x && snake->y[0] == fruit.y){
		snake->size++;
		//Clear the fruit
		PushCharXY(&tx0, ' ', fruit.x, fruit.y);

		//If our snake hit the fruit
		if(snake->can_addr == snakes[mySnakeIndex].can_addr)
		{
			UpdateFruit();
		}
	}
}


void SnakeCollision(snake_t * snake2){

	// Check snake1 head against all points in snake2
	int i;
	for(i = 0; i < snake2->size; i++){
		if(snakes[mySnakeIndex].x[0] == snake2->x[i] && snakes[mySnakeIndex].y[0] == snake2->y[i]){
			Snake_Remove();
		}
	}
}

void SelfCollision(snake_t * snake){

	int i;
	for(i = 1; i < snake->size; i++){
		if(snake->x[0] == snake->x[i] && snake->y[0] == snake->y[i]){
			Snake_Remove();
		}
	}
}

void Snake_GameOver(void){

	int i;
	char * name;

	// clear the screen
	Push_printf(&tx0,"\f");

	// show score
	Push_printf(&tx0,"Game Over! Your Score: %d\r\n", snakes[mySnakeIndex].size);

	// show everyones score
	for(i = 0; i < numSnakes; i++){
		name = CAN_NameLookup(snakes[i].can_addr);
		// Print their score
		Push_printf(&tx0,"%s : %d\r\n", name, snakes[i].size);
	}

	gameStatus = UINITIALIZED;
}

void Snake_Remove(void){

	snakes[mySnakeIndex].nextDirection = DEAD;
	UnregisterReceiverUART0(Snake_Receiver);
	LogEchoOn();

}


