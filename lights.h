/**
 * @file lights
 *
 * @defgroup lights Matt Montgomery's Tiva C LED Control Code for CAN
 * @ingroup CAN
 *
 * This is a code designed to control the led's on the Tiva C
 *
 * \n This is a code designed to turn off and on the led's on the Tiva C and their corresponding pins. lights_init enables all
 * three led pins, while the remaining functions are used to turn each one on and off. PF1 corresponds to the red led,
 * PF2 to blue, and PF3 to green.
 *
 *
 *  @date April 16, 2014
 *  @author Matthew Montgomery
 *  @version 1
 *
 * @{
 */

#ifndef LIGHTS_H_
#define LIGHTS_H_

/** lights_init enables and clears the led pins. This should be ran when the CAN project begins.
 * @param[in] void
 * @param[out] void
 *
 */
void lights_init(void);

/** red_on turns on pin PF1, the red led.
 * @param[in] void
 * @param[out] void
 *
 */
void red_on(void);

/** blue_on turns on pin PF2, the blue led.
 * @param[in] void
 * @param[out] void
 *
 */
void blue_on(void);

/** green_on turns on pin PF3, the green led.
 * @param[in] void
 * @param[out] void
 *
 */
void green_on(void);

/** red_off turns off pin PF1
 * @param[in] void
 * @param[out] void
 *
 */
void red_off(void);

/** blue_off turns off pin PF2
 * @param[in] void
 * @param[out] void
 *
 */
void blue_off(void);

/** green_off turns off pin PF3
 * @param[in] void
 * @param[out] void
 *
 */
void green_off(void);

/** all_off turns all of the pins off
 * @param[in] void
 * @param[out] void
 *
 */
void all_off(void);


/** @} */

#endif /* LIGHTS_H_ */
