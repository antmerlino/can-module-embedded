/**
 * @ingroup CAN
 * @defgroup CAN_Tunnel
 *
 * CAN_cmds.h
 *
 *  Created on: Apr 16, 2014
 *      Author: Rob
 *
 * @{
 */

#ifndef CAN_CMDS_H_
#define CAN_CMDS_H_
#include "cansys.h"
#include "subsys.h"

#ifndef CAN_RegisterReceiver
#define CAN_RegisterReceiver RegisterReceiver
#endif

struct CAN_device_t {
	uint8_t address;
	char name[7];
	tint_t last_msg;
};

extern struct CAN_device_t CAN_devices[CAN_MAX_DEVICES];

/** send the name of the device every 30 seconds

 */
#define CAN_SEND_NAME_PERIOD 30000

/**
 *
 * @param address
 * @param data
 */
void ReceiveTunnel(uint8_t address, uint8_t *data);

/**
 *
 */
void CAN_TunnelInit(void);

/** resolve a CAN address from a name
 *
 * Note: will resolve partial matches if they are the only ones which exist.
 * For example, if "muhlba" is a name and the string is just "mu" a match
 * will be found and address returned if no other devices have a name that starts
 * will "mu"
 *
 * If duplicate names exist then the higher numeric address will be returned
 * and a WARNING message will be printed.
 *
 * @param str pointer to name string
 * @return address index
 */
uint8_t CAN_AddrLookup(uint8_t * str);

char * CAN_NameLookup(uint8_t addr);

/**
 *
 * @param from
 * @param name
 */
void CAN_SaveName(uint8_t from, uint8_t *name);

/**
 *
 */
void CAN_SendMyName(void);

/**
 *
 * @return
 */
uint8_t CAN_GetObsoleteAddress(void);

/**
 *
 */
void CAN_PrintDeviceList(void);

/** @} */
#endif /* CAN_CMDS_H_ */
