/*
 * ADCReadTemperature.c
 *
 *  Created on: Apr 23, 2014
 *      Author: Rachana
 */

#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "ADCReadTemperature.h"

uint32_t ReadTemperature()
{
	uint32_t ui32ADC0Value[4];	//array that will be used for storing the data read from the ADC FIFO
	uint32_t ui32TempAvg;	//variable for storing the average of the temperature.
	uint32_t ui32TempValueC;	//store the temperature values for Celsius
	uint32_t ui32TempValueF;	//store the temperature values for Fahrenheit.

	//SysCtlClockSet(SYSCTL_SYSDIV_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);	//Set up the system clock to run at 40MHz

	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);	//enable the ADC0 peripheral
	ADCHardwareOversampleConfigure(ADC0_BASE, 64);	//Configures the hardware oversampling factor of the ADC

	ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);	//configure the ADC sequencer

	//Configure steps 0 - 2 on sequencer 1 to sample the temperature sensor (ADC_CTL_TS)
	ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_TS);
	ADCSequenceStepConfigure(ADC0_BASE, 1, 1, ADC_CTL_TS);
	ADCSequenceStepConfigure(ADC0_BASE, 1, 2, ADC_CTL_TS);
	ADCSequenceStepConfigure(ADC0_BASE,1,3,ADC_CTL_TS|ADC_CTL_IE|ADC_CTL_END);
	ADCSequenceEnable(ADC0_BASE, 1);	//enable ADC sequencer 1

	while(!ADCIntStatus(ADC0_BASE, 1, false))
	{
		ADCIntClear(ADC0_BASE, 1);	//Clear interrupt to proceed to data capture
		ADCProcessorTrigger(ADC0_BASE, 1);	//Trigger the ADC conversion

		//Wait for the conversion to be complete
		while(!ADCIntStatus(ADC0_BASE, 1, false))
		{
		}

		//The number of samples available in the hardware FIFO are copied into the buffer
		ADCSequenceDataGet(ADC0_BASE, 1, ui32ADC0Value);
		//Calculates the average of the temperature sensor data
		ui32TempAvg = (ui32ADC0Value[0] + ui32ADC0Value[1] + ui32ADC0Value[2] + ui32ADC0Value[3] + 2)/4;
		//calculate the Celsius value of the temperature
		ui32TempValueC = (1475 - ((2475 * ui32TempAvg)) / 4096)/10;
		ui32TempValueF = ((ui32TempValueC * 9) + 160) / 5;
	}
	return ui32TempValueC;
}
