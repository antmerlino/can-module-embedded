/**
 * ADCReadTemperature.h
 *
 *This module reads the internal temperature value of Tiva C in degree celsius and sends the value.
 *Internal temerature sensor can be selected by selecting ADC_CTL_TS.
 *
 *  Created on: Apr 23, 2014
 *      Author: Rachana
 */

#ifndef ADCREADTEMPERATURE_H_
#define ADCREADTEMPERATURE_H_

// use a fake ADC channel number to request temp data through the command interface
#define ADC_INTERNAL_TEMPERATURE_CHANNEL 50

uint32_t ReadTemperature();

#endif /* ADCREADTEMPERATURE_H_ */
