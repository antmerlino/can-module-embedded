/**
 * @defgroup CAN
 * cansys.h
 *
 *  Created on: Apr 15, 2014
 *      Author: Jehandad
 *
 *      This file is the first attempt to implement a base version of the can module
 *      This CAN Module will implement the protocol that we have discussed in class
 *      and is documented in the file "CAN_protocol.h". This file will extend the
 *      already developed functionality of subsystems and the timer module that is already implemented.
 *
 *      This file also borrows heavily from the file multi_tx.c / multi_rx.c from TI's tiva ware
 *      however is in no way dependent on the existence of that file.
 *
 * @version 1.2.1 added tunneling and name support.
 * @version 1.2.2 fixed bugs in name support, added more log messages
 * @version 1.2.3 fixed tunnel bug, added limit to addressed assigned by master and ability to replace oldest used address
 * @version 1.2.4 added "$CAN dev" command to print all known devices on the bus
 * @version 1.2.5 added analog features and simple LED commands
 *
 * @todo receive name message
 *      @{
 */

/*
 * We would have two message objects one for the transmission of the messages and the other for the reception of messages.
 * Later we might split them in to further groups (Two Tx and Two Rx, one each for command and function tx and rx). Depending on how chatty it gets on the bus
 */

#ifndef CANSYS_H_
#define CANSYS_H_

//////////////////////MMmmbbbb
#define CAN_VERSION 0x01020005u

//#define CAN_MASTER

/*
 * Throw an error if we are not on Tiva C
 */

#ifndef PART_TM4C123GH6PM
#error "Device must support CAN module "
#endif

#ifndef CAN_BASE
//#pragma message "Defaulting to CAN0"
#define CAN_BASE CAN0_BASE
#endif

#ifndef CANIO_PORT
//#pragma message "Defaulting to PORTB for CAN"
#define CANIO_PORT CANIO_PORT_B
#endif

#ifndef CAN_BITRATE
//#pragma message "Defaulting to 500k bitrate"
#define CAN_BITRATE 500000
#endif

#if CANIO_PORT == CANIO_PORT_B
#define GPIO_PERIPH SYSCTL_PERIPH_GPIOB
#define GPIO_TX		GPIO_PB5_CAN0TX
#define GPIO_RX		GPIO_PB4_CAN0RX
#define GPIO_PORT_BASE	GPIO_PORTB_BASE
#define GPIO_PINS		GPIO_PIN_4 | GPIO_PIN_5
#endif

#define CAN_MAX_DEVICES 32

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_can.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/can.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"

#include "random_int.h"

/* Enumerations to list the modes and the functionalities we support */

//This enumeration is determined from bit10 of the address field
enum MessageNumber
{
	TX_FUNCTION = 1,
	RX_FUNCTION,
	TX_ADDRESS,
	RX_ADDRESS,
	RX_ADDRESS_ALL,
	UNUSED_CAN_MO
};

//This enumeration would be determined from data byte 0. NA stands for not applicable (say a different mode etc, which makes the enum invalid
enum func
{
	TERM = 0,
	COMMAND,
	NAME,
	EEPROM,
	SENSOR,
	SNAKE,
	NA
};

// The following enum determines the second data bye (byte 1) for a command func message
//Copied verbatim from prof Muhlbier's spec
enum cmd
{
	BASE = 0,
	TURN_GREEN_LED_ON,
	TURN_GREEN_LED_OFF,
	TURN_RED_LED_ON ,
	TURN_RED_LED_OFF ,
	TURN_BLUE_LED_ON ,
	TURN_BLUE_LED_OFF,
	DEMO_LED,
	RENAME,
	LAST_CMD
};

enum sensors
{
	SENSOR_RAW = 0,
	SENSOR_GET_RAW,
	SENSOR_INT_TEMP,
	SENSOR_GET_INT_TEMP
};

#include "demoLED.h"
#include "lights.h"
#define CAN_GREEN_LED_ON() green_on()
#define CAN_GREEN_LED_OFF() green_off()
#define CAN_RED_LED_ON() red_on()
#define CAN_RED_LED_OFF() red_off()
#define CAN_BLUE_LED_ON() blue_on()
#define CAN_BLUE_LED_OFF() blue_off()
#define CAN_DEMO_LED() demoLED()

/**
 * The function InitCAN() would be the initializer function for the can module and would be called in the main function, where the rest of the initializations are being done
 *
 * The function would initialize the messages objects based on defines in the system.h If the entire message object stack is required then only one define would suffice.
 * Otherwise the user of the code would have to
 */

void CAN_Init(void);

//Functions implementing the protocol using the enumerations defined above

// The functions have a return value to indicate an error, though I am unsure how we can implement that in a non-blocking call

/**
 * Function MsgTerm will send the message pointed to by the array msg to the address src.
 * @param src The srcination address of the controller on the CAN bus that will receive the message
 * @param msg
 * @return error Code (Not implemented yet)
 */
int8_t MsgTerm(uint8_t src, uint8_t* msg);

/**
 * Function MsgCmd will send the command @param cmd to the controller on the CAN bus with address @param src
 * @param src
 * @param cmd
 * @return Error Code (Not implemented yet)
 */
int8_t MsgCmd(uint8_t src, enum cmd);

/**
 * The function MsgReadEEPROM reads the eeprom bytes from the controller with address @param src and
 * memory address @param address and returns the result in the array pointed to by @param data
 * @param src
 * @param address
 * @param data
 * @return  Error Code (Not implemented yet)
 */
int8_t MsgReadEEPROM(uint8_t src,uint16_t address,int8_t* data);

/**
 * The function MsgWriteEEPROM write the eeprom bytes pointed to by @param data to the EEPROM of the controller with address @param src and
 * at memory address @param address
 * @param src
 * @param address
 * @param data
 * @return  Error Code (Not implemented yet)
 */
int8_t MsgWriteEEPROM(uint8_t src,uint16_t address,int8_t* data);

/**
 * This function will implement the inherent functionality of our protocol with dynamic addressing
 *
 * @return  Error Code (Not implemented yet)
 */
int8_t FnReqAddress(void);

void CAN_RxAddr_Handler(void);
void CAN_RxAddrAll_Handler(void);
void CAN_RxFunc_Handler(void);

uint8_t CAN_TxAddressMode(uint8_t address, uint8_t * data);

uint8_t CAN_TxFunctionMode(uint16_t id, uint8_t * data);

uint8_t CAN_GetMyAddress(void);

uint8_t CAN_GetMessageObjectID(void);

void CAN_FreeMessageObjectID(uint8_t id);

/** dedicated address for sending a address mode message to all devices
 *
 */
#define CAN_ADDRESS_ALL 31
/** @} */ // CAN

// define dummy functions if they aren't defined above
// should be defined as
// #define CAN_GREEN_LED_ON() GreenLEDOn()
// or similar
#ifndef CAN_GREEN_LED_ON
#define CAN_GREEN_LED_ON()
#endif
#ifndef CAN_GREEN_LED_OFF
#define CAN_GREEN_LED_OFF()
#endif
#ifndef CAN_RED_LED_ON
#define CAN_RED_LED_ON()
#endif
#ifndef CAN_RED_LED_OFF
#define CAN_RED_LED_OFF()
#endif
#ifndef CAN_BLUE_LED_ON
#define CAN_BLUE_LED_ON()
#endif
#ifndef CAN_BLUE_LED_OFF
#define CAN_BLUE_LED_OFF()
#endif
#ifndef CAN_DEMO_LED
#define CAN_DEMO_LED()
#endif

#endif /* CANSYS_H_ */
