/*
 * CAN.h
 *
 *  Created on: Apr 16, 2014
 *      Author: Anthony
 */

#ifndef CAN_H_
#define CAN_H_


void CAN_Init();

void CAN_TestTx(uint8_t address, uint8_t * data);



#endif /* CAN_H_ */
