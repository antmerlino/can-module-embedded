#include <stdint.h>
#include "lights.h"
#include "inc/tm4c123gh6pm.h"

/*
 * This code includes functions to simply turn on any of the LED's found on and off the Tiva C microprocessor.
 *
 */
void lights_init(void)
{
    //
    // Enable the GPIO port that is used for the on-board LED.
    //
    SYSCTL_RCGC2_R = SYSCTL_RCGC2_GPIOF;

    //
    // Enables PF1 (Red), PF2 (Blue), and PF3 (Green)
    //
    GPIO_PORTF_DIR_R = 0x02 | 0x04 | 0x08;
    GPIO_PORTF_DEN_R = 0x02 | 0x04 | 0x08;
}

void red_on(void)
{
	// Turn on Red LED
	GPIO_PORTF_DATA_R |= 0x02;
}

void blue_on(void)
{
	// Turn on Blue LED
	GPIO_PORTF_DATA_R |= 0x04;
}

void green_on(void)
{
	// Turn on Green LED
	GPIO_PORTF_DATA_R |= 0x08;
}



void red_off(void)
{
	// Turn off Red LED
	GPIO_PORTF_DATA_R &= ~(0x02);
}

void blue_off(void)
{
	// Turn off Blue LED
	GPIO_PORTF_DATA_R &= ~(0x04);
}

void green_off(void)
{
	// Turn off Green LED
	GPIO_PORTF_DATA_R &= ~(0x08);
}

void all_off(void)
{
	// Turn off all of the LED's
	GPIO_PORTF_DATA_R &= ~(0x02);
	GPIO_PORTF_DATA_R &= ~(0x04);
	GPIO_PORTF_DATA_R &= ~(0x08);
}
