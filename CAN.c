/*
 * CAN.c
 *
 *  Created on: Apr 16, 2014
 *      Author: Anthony
 */

#include <stdint.h>
#include <stdbool.h>

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/interrupt.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/can.h"

#include "CAN.h"
#include "system.h"

tCANMsgObject sMsgObjectRx;
tCANMsgObject sMsgObjectTx;

#define myaddress 0

void CAN_Handler(void);

void CAN_Init(){

	// Enable register access to CAN1 Module
	SysCtlPeripheralEnable(SYSCTL_PERIPH_CAN0);

	// Enable the GPIO port A
	SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOE );
	SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOB );

	// Configure PA0 as CAN1 Rx
	GPIOPinConfigure(GPIO_PE4_CAN0RX);

	// Configure PA1 as CAN1 Tx
	GPIOPinConfigure(GPIO_PB5_CAN0TX);

	// Select the CAN function for these pins.
	GPIOPinTypeCAN( GPIO_PORTB_BASE, GPIO_PIN_5);
	GPIOPinTypeCAN( GPIO_PORTE_BASE, GPIO_PIN_4);

	// Initializes CAN1 Module, Clears memory objects
	CANInit(CAN0_BASE);

	// Set the bit rate to 250,000
	CANBitRateSet(CAN0_BASE, SysCtlClockGet(), 250000);

	CANIntClear(CAN0_BASE, 0xFFFFFFFF);
	CANIntRegister(CAN0_BASE, CAN_Handler);
	CANIntEnable(CAN0_BASE, CAN_INT_MASTER);

	// Enable the CAN bus
	CANEnable(CAN0_BASE);

	sMsgObjectRx.ui32MsgID = (0x1 << 10) | (myaddress << 5);
    sMsgObjectRx.ui32MsgIDMask = (0x1 << 10) | (0x3E0); // Bit 10 set and our address in bits 5 - 9
    sMsgObjectRx.ui32Flags = MSG_OBJ_USE_ID_FILTER | MSG_OBJ_RX_INT_ENABLE;
    CANMessageSet(CAN0_BASE, 1, &sMsgObjectRx,  MSG_OBJ_TYPE_RX);


}

void CAN_TestTx(uint8_t address, uint8_t * data) {
	sMsgObjectTx.ui32MsgID = (0x1 << 10) | (address << 5) | myaddress;
	sMsgObjectTx.ui32Flags =  MSG_OBJ_NO_FLAGS;
	sMsgObjectTx.ui32MsgLen = 8;
	sMsgObjectTx.pui8MsgData = data;
	CANMessageSet(CAN0_BASE, 2, &sMsgObjectTx, MSG_OBJ_TYPE_TX);
}

void CAN_Handler(void) {
	if(CANStatusGet(CAN0_BASE, CAN_STS_NEWDAT) == 1) {
		CANMessageGet(CAN0_BASE, 1, &sMsgObjectRx, true);
	}
}

/*void CAN_Open(){


	CANMessageSet(CAN0_BASE, 1, )
}*/

