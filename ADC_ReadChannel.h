/**
 * ADC_ReadChannel.h
 *
 *  This module receives a data value from the user. This value is the "Channel" that
 *  the ADC will operate at. The function sets up the appropriate channel for the user.
 *  All that is needed for the function to work is a 8-bit number from 0-11. This 
 *  Represents the channel that the ADC will operate at.
 *
 *  Created on: Apr 16, 2014
 *      Author: Lou Laptop
 */

#ifndef ADC_READCHANNEL_H_
#define ADC_READCHANNEL_H_

#define ADC_LAST_CHANNEL 11

uint32_t ADC_ReadChannel(uint8_t msg4Data);

#endif /* ADC_READCHANNEL_H_ */
