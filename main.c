#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"

//#include "CAN.h"
#include "system.h"
#include "timing.h"
#include "subsys.h"
#include "task.h"
#include "cansys.h"


//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
}
#endif


//*****************************************************************************
//
// Main 'C' Language entry point.  Toggle an LED using TivaWare.
// See http://www.ti.com/tm4c123g-launchpad/project0 for more information and
// tutorial videos.
//
//*****************************************************************************
int main(void)
{
	SysCtlClockSet(SYSCTL_SYSDIV_4|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|
					SYSCTL_OSC_MAIN);

	TimingInit();
	TaskInit();
	// Initialize system (including UART0 and UART1
	SystemInit();
	lights_init();
	CAN_Init();

    while(1)
    {
    	// SystemTick will call our receivers when there is data in the
		// buffers. We do it this way so that we aren't calling the
		// receivers from within the interrupt.
		SystemTick();
    }
}


